package com.example.polls.payload;

import java.util.List;

public class ListUserApiResponse {
    private Boolean success;
    private List<String> users;

    public ListUserApiResponse(Boolean success, List<String> users) {
        this.success = success;
        this.users = users;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<String> getUsers() {
        return users;
    }

    public void setMessage(List<String> users) {
        this.users = users;
    }
}