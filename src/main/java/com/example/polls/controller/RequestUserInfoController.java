package com.example.polls.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.polls.model.User;
import com.example.polls.payload.ListUserApiResponse;
import com.example.polls.repository.UserRepository;

@RestController
@RequestMapping("/api/userinfo")
public class RequestUserInfoController {
	
	@Autowired
	UserRepository userRepository;
	
	@GetMapping("/getusers")
	public ResponseEntity<?> listUsers() {
		List<User> users = userRepository.findAll();
		List<String> usernames = users.stream().map(user -> user.getName()).collect(Collectors.toList());
		return ResponseEntity.ok(new ListUserApiResponse(true, usernames));
	}
}
